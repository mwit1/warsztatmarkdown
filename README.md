# WarsztatMarkdown

To jest moje repozytorium na GitHub. Poniżej znajdziesz kilka elementów składni Markdown.

## Paragraf 1

Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam ac odio nec dolor blandit laoreet. Ut in ligula vel leo fermentum ultrices.

## Paragraf 2

**To jest pogrubienie tekstu,** *a to kursywa.* Możemy także ~~przekreślać tekst.~~

## Paragraf 3

> Cytat jest potężnym narzędziem retorycznym, które może podkreślić ważność pewnych myśli.

## Lista Numeryczna

1. Pierwszy element
   1. Podpunkt 1
   2. Podpunkt 2
2. Drugi element
   1. Podpunkt 1
   2. Podpunkt 2

## Lista Nienumeryczna

- Element 1
  - Podpunkt A
  - Podpunkt B
- Element 2
  - Podpunkt C
  - Podpunkt D

## Blok Kodu

```python
def hello_world():
    print("Hello, world!")

print("Cześć!")
```

![Kanye West](5721db073361db839025a74d86f1a3ef.jpg)
